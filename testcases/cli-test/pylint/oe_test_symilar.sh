#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

###################################
#@Author    :   guheping
#@Contact   :   867559702@qq.com
#@Date      :   2022/12/05
#@License   :   Mulan PSL v2
#@Desc      :   Test "pylint" command
###################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_test() {
    LOG_INFO "Start to prepare the test environment"
    DNF_INSTALL "pylint"
    LOG_INFO "End to prepare the test environment"
}

function run_test() {
    LOG_INFO "Start to run test."
    symilar -d 4 ./common/test.py ./common/test.py | grep "TOTAL"
    CHECK_RESULT $? 0 0 "L$LINENO: symilar -d No Pass"
    symilar --duplicates 4 ./common/test.py ./common/test.py | grep "TOTAL"
    CHECK_RESULT $? 0 0 "L$LINENO: symilar -d No Pass"
    symilar -i ./common/test.py ./common/test.py | grep "TOTAL"
    CHECK_RESULT $? 0 0 "L$LINENO: symilar -i No Pass"
    symilar --ignore-comments ./common/test.py ./common/test.py | grep "TOTAL"
    CHECK_RESULT $? 0 0 "L$LINENO: symilar --ignore-comments No Pass"
    symilar --ignore-docstrings ./common/test.py ./common/test.py | grep "TOTAL"
    CHECK_RESULT $? 0 0 "L$LINENO: symilar --ignore-docstrings No Pass"
    symilar --ignore-imports ./common/test.py ./common/test.py | grep "TOTAL"
    CHECK_RESULT $? 0 0 "L$LINENO: symilar --ignore-imports No Pass"
    LOG_INFO "End to run test."
}
function post_test() {
    LOG_INFO "Start to restore the test environment."
    DNF_REMOVE "$@"
    LOG_INFO "End to restore the test environment."
}

main "$@"
#!/usr/bin/bash

# Copyright (c) 2021. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.
####################################
#@Author    	:   wang-guangge
#@Contact   	:   wangguangge@huawei.com
#@Date      	:   2024-02-06 09:39:43
#@License   	:   Mulan PSL v2
#@Desc      	:   Syscare test
#####################################

source "${OET_PATH}"/libs/locallibs/common_lib.sh
# source ../common/common_lib.sh

# 需要预加载的数据、参数配置
function config_params() {
    LOG_INFO "Start to config params of the case."

    EXECUTE_T="1000m"

    remote_user="root"
    # remote_ip="172.168.240.185"
    remote_ip="172.168.115.178"
    remote_passwd="openeuler@123"
    remote_path="/opt/auto_hotpatch/remote_test_files"
    
    tmp_kernel_version_release_arch=$(uname -r)
    src_kernel_version_release=${tmp_kernel_version_release_arch%.*}
    name_version_release=kernel-${src_kernel_version_release}
    source_file=kernel-${src_kernel_version_release}.src.rpm
    debuginfo_file=kernel-debuginfo-${tmp_kernel_version_release_arch}.rpm
    

    declare -A release_branch_dic
    release_branch_dic=([oe1]="openEuler-20.03-LTS-SP3" [oe2003sp4]="openEuler-20.03-LTS-SP4" [oe2203sp1]="openEuler-22.03-LTS-SP1" [oe2203sp2]="openEuler-22.03-LTS-SP2" [oe2203sp3]="openEuler-22.03-LTS-SP3" [oe2203sp4]="openEuler-22.03-LTS-SP4" [oe2403]="openEuler-24.03-LTS" [oe2403sp1]="openEuler-24.03-LTS-SP1" [oe2203]="openEuler-22.03-LTS")
    release=${src_kernel_version_release##*.}
    branch=${release_branch_dic[${release}]}

    arch=${tmp_kernel_version_release_arch##*.}
    
    workspace="/root/kernel_patch"
    workspace_output=(${workspace}/output)
    workspace_build_root=(${workspace}/build_root)
    workspace_patch_file=(${workspace}/patch_file)
    workspace_output_history=(${workspace}/output_history)
    workspace_build_bak=(${workspace}/bak)

    touch /etc/yum.repos.d/openEuler_epol_update.repo
    dnf repolist|grep "^EPOL_UPDATE"
    result_value=$?
    if ((result_value !="0"));then
        echo "
[EPOL_UPDATE]
name=EPOL_UPDATE
baseurl=https://repo.openeuler.org/${branch}/EPOL/update/main/${arch}/
metadata_expire=1h
enabled=1
gpgcheck=1
gpgkey=http://repo.openeuler.org/${branch}/OS/${arch}/RPM-GPG-KEY-openEuler
" >>/etc/yum.repos.d/openEuler_epol_update.repo
    fi

    LOG_INFO "End to config params of the case."
}

# 测试对象、测试需要的工具等安装准备
function pre_test() {
    LOG_INFO "Start to prepare the test environment."
    DNF_INSTALL "syscare syscare-build"
    DNF_INSTALL "kpatch"

    # 安装编译内核需要的包
    DNF_INSTALL "asciidoc audit-libs-devel binutils-devel clang elfutils-devel elfutils-libelf-devel gtk2-devel java-1.8.0-openjdk java-1.8.0-openjdk-devel java-devel libbabeltrace-devel libcap-devel libcap-ng-devel libunwind-devel libzstd-devel llvm ncurses-devel net-tools newt-devel numactl-devel pciutils-devel perl-generators python3-devel python3-docutils rsync xmlto xz-devel"
    DNF_INSTALL "dwarves"
    DNF_INSTALL "OpenCSD libpfm-devel libtraceevent-devel llvm-devel"

    # 确认工作目录已存在，不存在则创建
    if [ ! -d ${workspace} ];then
        mkdir ${workspace}
        LOG_INFO "Create workspace succeed."
    else
        LOG_INFO "Workspace already exits."
    fi

    # 确认工作输出目录已存在，存在则清空；不存在则创建
    if [ ! -d ${workspace_output} ];then
        mkdir ${workspace_output}
        LOG_INFO "Create workspace output file succeed."
    else
        rm -rf ${workspace_output}/*
        LOG_INFO "Workspace output file already exits. Clear workspace output."
    fi

    # 确认历史工作输出目录已存在，不存在则创建
    if [ ! -d ${workspace_output_history} ];then
        mkdir ${workspace_output_history}
        LOG_INFO "Create workspace history output file succeed."
    else
        LOG_INFO "Workspace history output file already exits."
    fi

    # 确认工作编译目录已存在，存在则清空；不存在则创建
    if [ ! -d ${workspace_build_root} ];then
        mkdir ${workspace_build_root}
        LOG_INFO "Create workspace build root file succeed."
    else
        rm -rf ${workspace_build_root}/*
        LOG_INFO "Workspace build root file already exits. Clear build root file."
    fi

    # 确认patch目录已存在，存在则清空；不存在则创建
    if [ ! -d ${workspace_patch_file} ];then
        mkdir ${workspace_patch_file}
        LOG_INFO "Create workspace patch file succeed." 
    else
        rm -rf ${workspace_patch_file}/*
        LOG_INFO "Workspace patch file already exits. Clear patch file."
    fi

    # 下载相关src和debuginfo包
    if [ ! -f ${workspace}/${source_file} ];then
        echo ${workspace}/${source_file}
        wget -P ${workspace} https://repo.openeuler.org/${branch}/source/Packages/${source_file}
    fi
    if [ ! -f ${workspace}/${debuginfo_file} ];then
        echo ${workspace}/${debuginfo_file}
        wget -P ${workspace} https://repo.openeuler.org/${branch}/debuginfo/${arch}/Packages/${debuginfo_file}
    fi
    test $(ls -l ${workspace} | grep ${source_file} | wc -l) -eq 1
    CHECK_RESULT $? 0 0 "Prepare ${source_file} fail." 1
    test $(ls -l ${workspace} | grep ${debuginfo_file} | wc -l) -eq 1
    CHECK_RESULT $? 0 0 "Prepare ${debuginfo_file} fail." 1

    # 获取远程待测试patch文件
    SSH_SCP $remote_user@$remote_ip:$remote_path/$branch/* ${workspace_patch_file} "$remote_passwd"
    ls ${workspace_patch_file}

    # 确认patch文件已获取到
    if [ ! -d ${workspace_patch_file} ];then
        LOG_ERROR "Workspace has no patch files. Please check if patch file has been transferred." && return 1
    fi

    if [ $(ls ${workspace_patch_file} | wc -l) -ne 1 ]; then
        LOG_ERROR "Workspace patch files error." && return 1
    fi

    if [ $(ls ${workspace_patch_file} | grep 'CVE-' | wc -l) -ne 1 ]; then
        LOG_ERROR "Workspace patch files error." && return 1
    fi

    cve_id=$(ls ${workspace_patch_file} | grep 'CVE-')
    
    patch_name=SGL_$(sed "s/-/_/g" <<< "$cve_id")
    


    LOG_INFO "End to prepare the test environment."
}


# 测试点的执行
function run_test() {
    LOG_INFO "Start to run test."
    
    
    # 获取patch文件名，按文件名从小到大顺序以' '进行拼接
    patch_file=""
    workspace_cve_patch_file=${workspace_patch_file}/${cve_id}
    for file in $(ls ${workspace_cve_patch_file} | sort -n)
    do
        if [ "${file##*.}" = "patch" ]; then
            patch_file=("${patch_file} ${workspace_cve_patch_file}/${file}")
        fi
    done

    LOG_INFO "This is a syscare patch test for ${cve_id} with ${patch_file}."

    
    syscare build --patch-name ${patch_name}  --source ${workspace}/${source_file} --debuginfo ${workspace}/${debuginfo_file} --patch ${patch_file} --output ${workspace_output} --build-root ${workspace_build_root}
    if [ $? -ne 0 ]; then
        LOG_ERROR "Syscare build fail."
        return 1
    fi

    hotpatch_rpm=$(ls ${workspace_output} | grep patch-${name_version_release}-${patch_name}-1-1)
    hotpatch_src_rpm=${name_version_release}-${patch_name}-1-1.src.rpm
    hotpatch_rpm_name=patch-${name_version_release}-${patch_name}-1-1.${arch}
    hotpatch_name=$name_version_release/${patch_name}-1-1

    # 存储制作成功的热补丁文件
    cp ${workspace_output}/${hotpatch_rpm} ${workspace_output}/${hotpatch_src_rpm} ${workspace_output_history}/
    CHECK_RESULT $? 0 0 "Store ${hotpatch_rpm} and ${hotpatch_src_rpm} to ${workspace_output_history} fail." 0

    # 安装热补丁测试默认状态
    rpm -ivh ${workspace_output}/${hotpatch_rpm}
    CHECK_RESULT $? 0 0 "Install ${hotpatch_rpm} fail." 1
    syscare list | grep NOT-APPLIED | grep ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare list fail." 1
    syscare status ${hotpatch_name} | grep NOT-APPLIED
    CHECK_RESULT $? 0 0 "Syscare status fail." 1
    syscare info ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare info fail." 1
    

    # 激活，检查补丁生效
    syscare apply ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare apply fail." 1
    syscare active ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare active fail." 1
    syscare list | grep ${hotpatch_name} | grep -w ACTIVED
    CHECK_RESULT $? 0 0 "Syscare apply fail." 1

    # 去激活，检查补丁不生效
    syscare deactive ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare deactive fail." 1
    syscare list | grep ${hotpatch_name} | grep -w DEACTIVED
    CHECK_RESULT $? 0 0 "Syscare deactive fail." 1

    # 检查补丁其他管理流程
    syscare active ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare active fail." 1
    syscare list | grep ${hotpatch_name} | grep -w ACTIVED
    CHECK_RESULT $? 0 0 "Syscare list fail." 1
    syscare remove ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare remove fail." 1
    syscare list | grep ${hotpatch_name} | grep NOT-APPLIED
    CHECK_RESULT $? 0 0 "Syscare list fail." 1
    syscare save
    CHECK_RESULT $? 0 0 "Syscare save fail." 1
    syscare apply ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare apply fail." 1
    syscare list | grep ${hotpatch_name} | grep -w ACTIVED
    CHECK_RESULT $? 0 0 "Syscare list fail." 1
    syscare restore
    CHECK_RESULT $? 0 0 "Syscare restore fail." 1
    syscare list | grep ${hotpatch_name} | grep -w NOT-APPLIED
    CHECK_RESULT $? 0 0 "Syscare list fail." 1

    # accept后，检查补丁生效
    syscare apply ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare apply fail." 1
    syscare accept ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare accept fail." 1
    syscare list | grep ${hotpatch_name} | grep ACCEPTED
    CHECK_RESULT $? 0 0 "Syscare accept fail." 1
    # livepatch -q检查热补丁激活是否依赖除'vmlinux'以外的模块
    livepatch -q
    livepatch_q_info=$(livepatch -q | head -n -1 | xargs)
    module_ko=${livepatch_q_info##*Denpendency:}
    ko=$(echo ${module_ko} | tr ' ' '\n')
    require_ko_num=$(echo $ko | grep -v 'vmlinux' | wc -l)

    # 删除热补丁
    syscare remove ${hotpatch_name}
    CHECK_RESULT $? 0 0 "Syscare remove fail." 1
    rpm -e ${hotpatch_rpm_name}
    CHECK_RESULT $? 0 0 "Remove hotpatch fail." 1
    rpm -qa | grep ${hotpatch_rpm_name}
    CHECK_RESULT $? 1 0 "Remove hotpatch rpm fail." 1

    # 确认热补丁是否依赖其他模块
    CHECK_RESULT ${require_ko_num} 0 0 "Syscare active require modules: ${module_ko}" 1

    LOG_INFO "End to run test."
}

# 后置处理，恢复测试环境
function post_test() {
    LOG_INFO "Start to restore the test environment."

    if [ ! -d "${workspace_build_bak}/${cve_id}" ];then
        mkdir -p "${workspace_build_bak}/${cve_id}"
        LOG_INFO "Create workspace build bak directory succeed."
    fi
    # 复制编译日志到指定目录下
    find ${workspace_build_root} -type f -name "build.log" -exec cp -- "{}" "${workspace_build_bak}/${cve_id}" \;
    find ${workspace_patch_file} -type f -name "*.patch" -exec cp -- "{}" "${workspace_build_bak}/${cve_id}" \;
    #DNF_REMOVE
    # 删除工作目录下的patch文件和输出文件
    rm -rf ${workspace_patch_file}
    rm -rf ${workspace_output}
    # rm -rf ${workspace_build_root}/*

    # 删除已安装热补丁
    rpm -qa | grep ${hotpatch_rpm_name}
    if [ $? -eq 0 ]; then
        rpm -e ${hotpatch_rpm_name}
    fi
    
    rm -rf /etc/yum.repos.d/openEuler_epol_update.repo

    LOG_INFO "End to restore the test environment."
}

main "$@"
